const db = require('../../models');

async function getShoppingCart(userId) {
    let shoppingCart = await db.ShoppingCart.findOne({
        where: {
            userId: userId
        },
        include: {
            model: db.Book,
            as: 'books'
        }
    });
    if (!shoppingCart) {
        shoppingCart = await db.ShoppingCart.create({userId: userId});
        return [shoppingCart, 201];
    }
    return [shoppingCart, 200];
}

module.exports = getShoppingCart;
