module.exports = (sequelize, DataTypes) => {
    const Book = sequelize.define('Book', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        isbn: {
            type: DataTypes.STRING,
            allowNull: false
        },
        author: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        price: {
            type: DataTypes.DECIMAL,
            allowNull: true
        }
    });
    Book.associate = function(models) {
        Book.belongsToMany(models.Order, {through: models.orderBook, foreignKey: 'bookId', as: 'orderBooks'});
        Book.belongsToMany(models.ShoppingCart, {through: models.shoppingCartBook, foreignKey: 'bookId', as: 'shoppingCarts'});
    }
    return Book;
}
