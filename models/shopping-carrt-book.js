module.exports = (sequelize, DataTypes) => {
    const ShoppingCartBook = sequelize.define('shoppingCartBook', {
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
    }, {
        tableName: 'ShoppingCartBooks'
    });

    ShoppingCartBook.associate = function(models) {

    }

    return ShoppingCartBook;
}
