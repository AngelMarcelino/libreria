module.exports = (sequelize, DataTypes) => {
    const Order = sequelize.define('Order', {
        date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        shippingCompany: {
            type: DataTypes.ENUM({
                values: ['DHL', 'FEDEX', 'ESTAFETA', 'UPS']
            }),
            allowNull: false
        },
        shippingMethod: {
            type: DataTypes.ENUM({
                values: ['FAST', 'NORMAL', 'SLOW']
            }),
            allowNull: false
        },
        parentOrder: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        orderData: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    });

    Order.associate = function(models) {
        Order.belongsTo(models.User, { foreignKey: 'userId', as: 'user'});
        Order.belongsToMany(models.Book, {through: models.orderBook, foreignKey: 'orderId', as: 'books'});
    }

    return Order;
}