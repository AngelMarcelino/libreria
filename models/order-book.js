module.exports = (sequelize, DataTypes) => {
    const OrderBook = sequelize.define('orderBook', {
        price: {
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        quantity: {
            type: DataTypes.INTEGER
        },

    },
        {
            tableName: 'OrderBooks'
        });

    OrderBook.associate = function(models) {

    }

    return OrderBook;
}