module.exports = (sequelize, DataTypes) => {
    const ShoppingCart = sequelize.define('ShoppingCart', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    });

    ShoppingCart.associate = function(models) {
        ShoppingCart.belongsToMany(models.Book, {through: models.shoppingCartBook, foreignKey: 'shoppingCartId', as: 'books'});
        ShoppingCart.belongsTo(models.User, { foreignKey: 'userId'});
    }

    return ShoppingCart;
}