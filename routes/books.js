const express = require('express');
const router = express.Router();
const db = require('../models');

router.get('/', async (req, res) => {
    const books = await db.Book.findAll();
    res.send(books);
});

router.post('/', async (req, res) => {
    let book = req.body;
    book = await db.Book.create({
        ...book
    });
    res.status(201)
        .send(book);
});

module.exports = router;
