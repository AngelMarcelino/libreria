const express = require('express');
const router = express.Router();
const db = require('../models');
const _ = require('lodash')
const getShoppingCart = require('../services/shipping-cart/get-shopping-cart');

/* GET users listing. */
router.get('/', async function(req, res, next) {
  const users = await db.User.findAll();
  res.send(users.toJSON());
});



router.get('/:userId/shoppingCart', async (req, res) => {
  let userId = 0;
  if (!(userId = parseInt(req.params.userId))) {
    res.status(400)
        .send('Invalid url param');
  } else {
    const shoppingCartStatusCode = await getShoppingCart(userId);
    res.status(shoppingCartStatusCode[1])
        .send(shoppingCartStatusCode[0]);
  }
});

router.put('/:userId/shoppingCart', async (req, res) => {
  let userId = 0;
  if (!(userId = parseInt(req.params.userId))) {
    req.status(400)
        .send('Invalid url params');
  } else {
    const [shoppingCart, statusCode] = await getShoppingCart(userId);
    await db.shoppingCartBook.destroy({
      where: {
        shoppingCartId: shoppingCart.id
      }
    });
    const newShoppingCartBooks = req.body;
    const created = await db.shoppingCartBook.bulkCreate([
        ...newShoppingCartBooks
    ], { fields: ['quantity', 'bookId', 'shoppingCartId']});
    res.status(statusCode)
        .send(created);
  }
});

module.exports = router;
