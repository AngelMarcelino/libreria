const express = require('express');
const router = express.Router();
const db = require('../models');
const getShoppingCart = require('../services/shipping-cart/get-shopping-cart');

router.post('/pay/:userId', async (req, res) => {
    let userId = 0;
    if (!(userId = parseInt(req.params.userId))) {
        req.status(400)
            .send('Invalid url params');
    } else {
        const body = req.body;
        const [shoppingCart, status] = await getShoppingCart(userId);

        const order = await db.Order.create({
            date: new Date(),
            orderData: JSON.stringify(body),
            shippingMethod: body.shippingMethod,
            shippingCompany: body.shippingCompany,
            userId: userId
        });
        const orderBooks = [];
        for (let i = 0; i < shoppingCart.books.length; i++) {
            const currentBook = shoppingCart.books[i];
            const createdOrderBook = await db.orderBook.create({
                quantity: currentBook.shoppingCartBook.quantity,
                price: currentBook.price,
                bookId: currentBook.id,
                orderId: order.id
            });
            orderBooks.push(createdOrderBook);
        }
        res.send({
            order: {
                ...order,
                orderBooks: orderBooks
            }
        });
    }
});

module.exports = router;
