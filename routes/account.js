const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const key = require('../constants/key').jwtKey;
const db = require('../models');

router.post('/token', async (req, res) => {
    const loginModel = req.body;
    const user = await db.User.findOne({ where: {
            email: loginModel.email,
            password: loginModel.password
        }
    });
    if (user === null) {
        res.status(404)
            .send('User not found');
    } else {
        const token = jwt.sign({sub: user.id, name: user.name}, key, {expiresIn: '1h'});
        res.send(token);
    }
});

router.post('/register', async (req, res) => {
    const registerModel = req.body;
    const user = await db.User.create({
        ...registerModel
    });
    res.status(201)
        .send(user);
});

module.exports = router;
