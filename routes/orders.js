const express = require('express');
const router = express.Router();
const db = require('../models');

router.get('/', async (req, res) =>{
    let orders = await db.Order.findAll({
        include: [{
            model: db.Book,
            as: 'books'
        }, {
            model: db.User,
            as: 'user'
        }]
    });
    res.send(orders);
});

module.exports = router;
