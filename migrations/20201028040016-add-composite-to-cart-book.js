'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addConstraint('ShoppingCartBooks', {
      type: 'unique',
      name: 'unique_bookId_shoppingCartId',
      fields: ['shoppingCartId', 'bookId']
    });

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    await queryInterface.removeConstraint('ShoppingCartBooks', 'unique_bookId_shoppingCartId')
  }
};
