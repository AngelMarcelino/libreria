'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER
      },
      email: {
        type: Sequelize.DataTypes.STRING
      },
      name: {
        type: Sequelize.DataTypes.STRING
      },
      password: {
        type: Sequelize.DataTypes.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE
      }
    });
    await queryInterface.createTable('Books', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      title: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
      },
      isbn: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
      },
      author: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false
      }
    });
    await queryInterface.createTable('ShoppingCarts', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      userId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        },
        allowNull: true
      }
    });

    await queryInterface.createTable('ShoppingCartBooks', {
      quantity: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false
      },
      bookId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'Books',
          key: 'id'
        },
        allowNull: false
      },
      shoppingCartId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'ShoppingCarts',
          key: 'id'
        },
        allowNull: false
      }
    });
    await queryInterface.createTable('Orders', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      date: {
        type: Sequelize.DataTypes.DATE,
        allowNull: false,
      },
      shippingCompany: {
        type: Sequelize.DataTypes.ENUM({
          values: ['DHL', 'FEDEX', 'ESTAFETA', 'UPS'],
          allowNull: false
        })
      },
      shippingMethod: {
        type: Sequelize.DataTypes.ENUM({
          values: ['Fast', 'Normal', 'Slow']
        })
      },
      parentOrder: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: true
      },
    });
    await queryInterface.createTable('OrderBooks', {
      price: {
        type: Sequelize.DataTypes.DECIMAL,
        allowNull: false,
      },
      quantity: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false
      },
      bookId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'Books',
          key: 'id'
        },
        allowNull: false
      },
      orderId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'Orders',
          key: 'id'
        },
        allowNull: false
      }
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
    await queryInterface.dropTable('Books');
    await queryInterface.dropTable('ShoppingCarts');
    await queryInterface.dropTable('ShoppingCartBooks');
    await queryInterface.dropTable('Orders');
    await queryInterface.dropTable('OrderBooks');
  }
};