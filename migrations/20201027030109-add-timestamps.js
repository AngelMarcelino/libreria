'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn('Books', 'createdAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('Books', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('ShoppingCarts', 'createdAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('ShoppingCarts', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('ShoppingCartBooks', 'createdAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('ShoppingCartBooks', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('Orders', 'createdAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('Orders', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('OrderBooks', 'createdAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });

    await queryInterface.addColumn('OrderBooks', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DataTypes.DATE
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

    await queryInterface.removeColumn('Books', 'createdAt');
    await queryInterface.removeColumn('Books', 'updatedAt');

    await queryInterface.removeColumn('ShoppingCarts', 'createdAt');
    await queryInterface.removeColumn('ShoppingCarts', 'updatedAt');

    await queryInterface.removeColumn('ShoppingCartBooks', 'createdAt');
    await queryInterface.removeColumn('ShoppingCartBooks', 'updatedAt');

    await queryInterface.removeColumn('Orders', 'createdAt');
    await queryInterface.removeColumn('Orders', 'updatedAt');

    await queryInterface.removeColumn('OrderBooks', 'createdAt');
    await queryInterface.removeColumn('OrderBooks', 'updatedAt');

  }
};
