const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const keys = require('../../constants/key');

const options = {};

options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
options.secretOrKey = keys.jwtKey;
passport.use(new JwtStrategy(options, function (jwt_payload, done) {
    console.log('hola');
    return done(null, jwt_payload);
}));
