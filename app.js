const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
require('./infrastructure/security/configure-passport');
const passport = require('passport');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const booksRouter = require('./routes/books');
const shoppingCartRouter = require('./routes/shopping-cart');
const accountRouter = require('./routes/account');
const orderRouter = require('./routes/orders');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/books', booksRouter);
app.use('/account', accountRouter);
app.use('/shoppingCart', shoppingCartRouter);
app.use('/orders', orderRouter);

module.exports = app;

